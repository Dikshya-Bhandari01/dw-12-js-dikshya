let names=['nitan', 'ram', 'laxman', 29, true]
//            0   ,   1  ,   2     ,  3,  4
//array is used to store data of different type or same type
//retrieving all elements
console.log(names)
//retrieving specific elements
console.log(names[0]);    //alt + shift + down=> copy
console.log(names[1]);    //control + d=> to select
console.log(names[2]);
//change element of array
names[1]='hari';
console.log(names)


let nam=['ram', 'sita', 30, false]
console.log(nam);
console.log(nam[2]);
nam[3]='mango';
console.log(nam);