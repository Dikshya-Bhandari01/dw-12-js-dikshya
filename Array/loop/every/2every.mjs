//check whether we have all even number in the list= [2,4,9,6]
let ar1=[2,4,9,6]
let isAllEvenNum=ar1.every((value,index)=>{
  if(value%2===0){
    return true
  }
})
console.log(isAllEvenNum)
//by default it will be false 

//check whether all students get pass mark from the list [40, 30,80] have pass marks is 40
let marks=[40, 30, 80]
let hasAllStudentsPass=marks.every((value, index)=>{
    if(value>=40){
        return true
    }
})
console.log(hasAllStudentsPass);