//let info=["nitan", 29, 65, false]
//object is used to store multiple data
//object is same as array but it has information of a data
//it has 3 part
//property=key value

let info = {
  name: "nitan",
  age: 29,
  weight: 65,
  isMarried: false,
};
console.log(info.name);
console.log(info.age);
console.log(info.weight);
console.log(info.isMarried);
info.age=30;
console.log(info); //{name:'nitan', age:30, weight:65, isMarried:false}
delete info.weight;
console.log(info); //{name:'nitan', age:30, isMarried: false}
