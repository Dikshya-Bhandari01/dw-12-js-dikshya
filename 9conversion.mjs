//string to number and number to string;
// let num =1234;
// let strNum= String(num); // "1234";
// console.log(num); // 12334;
// console.log(strNum);
console.log(String(1234)); //"1234"

//conversion to  string to Number
console.log(Number("1234")); //1234

//conversion to boolean
/* console.log(Boolean("nitan")) 
console.log(Boolean("a")); 
console.log(Boolean('0')) 
console.log(Boolean(" ")) 
console.log(Boolean(""))//false
console.log(Boolean(0))// false
console.log(Boolean(1)) */


console.log(Boolean(0.00001));
console.log(Boolean(2));


/* 
all empty are falsy value
string
"" => falsy
all are truthy

number
0=> falsy
all are truthy
*/

