// &&, ||, !
// && true if all are true
// || true if one is true
console.log(true && false && true) //false
console.log(true || false) //true
console.log(true||true)//true
console.log(false||false)//false
console.log(false|| true || false)//true
console.log(!true) //false
console.log(!false) //true