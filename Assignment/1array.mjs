//map method

// ***********
// let list=["a", "b", "c"];
// let list1=list.map((value, i)=>{
//   return `${value}${i}`
// });
// console.log(list1)   //['a0', 'b1', 'c2']
//****************

// ****************
// [1,2,3,4]=> [2,4,6,8]
// let arr=[1,2,3,4]
// let arr1=arr.map((value,index)=>{
//     return value * 2;
// })
// console.log(arr1)
//****************

//****************
//[1,2,3]=>[11,12,13]
// let input=[1,2,3]
// let output=input.map((value,i)=>{
//  return value + 10;
// });
// console.log(output)
//****************


//****************
//["my" , "name", "is"]=> ["MY", "NAME","IS"]
// let inStr=["my","name","is"]
// let outStr=inStr.map((value,i)=>{
// return value.toUpperCase()
// })
// console.log(outStr)
//****************


//****************
//["my" , "name", "is"]=>["MYN", "NAMEN","ISN"]
// let addN=["my", "name", "is"]
// let _addN=addN.map((value, i)=>{
//     return `${value.toUpperCase()}N`;
// })
// console.log(_addN)
//****************

//[1,3,4,5]=>[100, 300,0, 500] 
// let input=[1,3,4,5]
// let output=input.map((value, i)=>{
//    if(value%2===0){
//     return value*0
//    }else{
//     return value*100
//    }
// })
// console.log(output)

//*********/
//[1,2,3,4]=>[2,0,6,0] here odd index is multiplied by 0 and even index is multiplied by2
// let input=[1,2,3,4]
// let output=input.map((value,i)=>{
//     if(i%2===0){
//         return value*2
//     }else{
//         return value*0
//     }
// });
// console.log(output)

//***********/

//["n", "i", "t", "a", "n"]=>["N", "i", "t", "a", "n"]
let str=["n", "i","T","A","n"]
let _str=str.map((value,i)=>{
    if(i===0){
        return value.toUpperCase()
    }else{
        return value.toLowerCase()
    }
})
console.log(_str)

