// find sum of all product
let products = [
  {
    name: "earphone",
    price: 1000,
  },
  {
    name: "battery",
    price: 2000,
  },
  {
    name: "charger",
    price: 500,
  },
];
let productsPrice = products.map((value, i) => {
  return value.price;
});
let sumOfAllProducts = productsPrice.reduce((pre, cur) => {
  return pre + cur;
}, 0);
console.log(sumOfAllProducts);
