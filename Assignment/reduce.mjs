//find the sum of all even element of array
let list = [1, 2, 3, 4];
let evenList = list.filter((value, i) => {

  if (value % 2 === 0) {
    return true;
  } else {
    return false;
  }
});
let sumOfEven = evenList.reduce((pre, cur) => {
  return pre + cur;
}, 0);
console.log(sumOfEven);