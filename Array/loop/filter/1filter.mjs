 /* 
 [1,2,3,4]=[1,2] ok
 [1,2,3,4]=[1,4] ok
 [1,4]=[1,4,4]=> not okay
 [1,2,3,4,]=>[1,5]=> not okay
 */
let ar1=[1,2,3,4]    //we need output ar2=[2,4]
let ar2=ar1.filter((value,index)=>{
  if(value%2===0){
    return true;
  }else{
    return false;
  }
})
console.log(ar2)

//by default return will be false if there is no return execution