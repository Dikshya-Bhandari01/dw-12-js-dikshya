// ==, === (difference)
/* 
==  : same value herxa
*/

/* 
=== : same value and same type
*/

console.log(1==1); //true
console.log(1===1); //true

console.log("1"==1); //true
console.log("1"===1); //false