//object to array 
let info={
    name:"nitan",
    age:29,
    ismarried:false,
};
//key ["name", "age", "isMarried"]
//["nitan", 29, false]
//["name", "nitan"], ["age", 29], ["isMarried", false] properties, entries

let keysArray=Object.keys(info);
console.log(keysArray);

let valueArray=Object.values(info);
console.log(valueArray);

let propertiesArray= Object.entries(info);
console.log(propertiesArray)