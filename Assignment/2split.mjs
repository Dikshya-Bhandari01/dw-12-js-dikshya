//split is used to convert string to array
let str="nitan"
console.log(str.split("t")); //[ 'ni', 'an' ]
console.log(str.split("i")); //[ 'n', 'tan' ]
console.log(str.split("a")); //[ 'nit', 'n' ]
console.log("dikshya".split(""))   //[d,i,k,s,h,y,a]
//split=> string=> array

//join=> array=> string
//["d", "i", "k","s", "h","y", "a"]=>join("*")
let input=["d","i","k","s","h","y","a"]
console.log(input.join("*"));
console.log(input.join(""))

//"my name is dikshya bhandari"=>["my", "name", "is", "dikshya", "bhandari"]
let  string="my name is Dikshya Bhandari"
console.log(string.split(" "))

//["Dikshya", "Bhandari",  "is",  "my",  "name"]=>"my name is dikshya bhandari"
let arr=["Dikshya", "Bhandari",  "is",  "my",  "name"]
console.log(arr.join(" "));