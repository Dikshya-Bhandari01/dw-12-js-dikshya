/* 
define function
call function
*/

let fun1=function(){      //function define
    let num1=1;
    let num2=2;
    console.log(num1+num2)
}

fun1()   //function calling => function name ()
console.log("out of function");
fun1();  //function calling => function name()
console.log("111")
/* 
3
out of function
*/