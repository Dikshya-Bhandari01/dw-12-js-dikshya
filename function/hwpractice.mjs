// a) make a arrow function name is18 pass a value ,the function must return true if the given age is 18 otherwise return false

let is18 = function (age) {
  if (age === 18) {
    return true;
  } else {
    return false;
  }
};
let __is18 = is18(20);
console.log(__is18);

//	b) make a arrow function named isGreaterThan18 , pass a value , the function must return true if the given age is greater or equals to 18 otherwise false
let isGreaterThan18 = function (age) {
  if (age >= 18) {
    return true;
  } else {
    return false;
  }
};
let _isGreaterThan18 = isGreaterThan18(18);
console.log(_isGreaterThan18);

//c) make a arrow function that take a number and return you can enter room only if the enter number is less than 18 else you can not enter

let number = function (num) {
  if (num < 18) {
    return "you can enter room";
  } else {
    return "you cannot enter room";
  }
};
let _number = number(15);
console.log(_number);

//	d) make a arrow function named isEven , pass a value, that retrun true if the given number is even else return false

let isEven = function (num) {
  let _isEven = num % 2;
  if (_isEven === 0) {
    return true;
  } else {
    return false;
  }
};
let __isEven = isEven(6);
console.log(__isEven);

//	e) make a arrow function that takes 3 input as number and return average of given number
let avg = function (num1, num2, num3) {
  let _avg = (num1 + num2 + num3) / 3;
  return _avg;
};
let _avg = avg(5, 10, 15);
console.log(_avg);

//f) make a arrow function that takes one input as number and return "category1" for number range from 1 to10,  return "category2" for number range from 11 to 20, return "category3" for number range form 21 to 30
let categoryTypes = function (num) {
  if (num >= 1 && num <= 10) {
    return "category1";
  } else if (num >= 11 && num <= 20) {
    return "category2";
  } else if (num >= 21 && num <= 30) {
    return "category3";
  }
};
let _categoryTypes = categoryTypes(20);
console.log(_categoryTypes);

/*  g) make a arrow function that takes a input as number  that perform
if age [upto 17],  return your ticket is free
if age[18 to 25 ], return  your ticket cost 100
if age[>26],  return your ticket cost 200
if age===26 return your ticket is 150 */

let ticketCost = function (age) {
  if (age <= 17) {
    return "your ticket is free";
  } else if (age >= 18 && age <= 25) {
    return "your ticket cost 100";
  } else if (age > 26) { 
    return "your ticket cost 200";
  } else if (age == 26) {
    return "your ticket is 150";
  }
};
console.log(ticketCost(26));

//  h)  make a function that take a number
//if number>=3 console i am greater or equal to 3
//if number ===3 console i am 3
//if number<3 console i am less than3
//else show i am other

let num = function (n) {
  if (n >= 3) {
    console.log("i am grater or equal to 3");
  } else if (n === 3) {
    console.log("i am 3");
  } else {
    console.log("i am less than 3");
  }
};
num(4);

//  i) make a function that takes input as number and return output You can watch movies if input is greater or equal to 18 else return "You are not authorized to watch movies

let watchMovie = function (age) {
  if (age >= 18) {
    return "You can watch movies";
  } else {
    return "you are not authorized to watch movies";
  }
};
let _watchMovie = watchMovie(15);
console.log(_watchMovie);
