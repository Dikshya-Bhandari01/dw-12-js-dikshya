//[1,2,3,4]=>[2,4,6,8]
let fun1=(input)=>{
    let output=input.map((value,index)=>{
return value*2
    });
    return output;
}
let _fun1=fun1([1,2,3,4])
console.log(_fun1);

//[1,2,3]=>[11,12,13]
let fun2=(input1)=>{
    let output1=input1.map((value,index)=>{
        return value + 10
    })
    return output1
}
let _fun2=fun2([1,2,3]);
console.log(_fun2)

//["my", "name", "is"]=>["MY", "NAME", "IS"]
let str=(input2)=>{
 let output2=input2.map((value,i)=>{
   return `${value.toUpperCase()}`
 })
 return output2
}
let _str=str(["my", "name", "is"])
console.log(_str)

//["my", "name", "is"]=>["MYN", "NAMEN", "ISN"]
let str1=(input3)=>{
    let output3=input3.map((value,index)=>{
return `${value.toUpperCase()}N`
    })
    return output3
}
let _str1=str1(["my", "name","is"])
console.log(_str1)